// firebaseConfig.js
import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';

// Configuración de tu proyecto Firebase
const firebaseConfig = {
  apiKey: 'AIzaSyDRHpgZW2Ra81YuM_UXhJzawMQ64mLVjZY ',
  authDomain: '683471465239-0dlltou5labudhlqdv0u3c0vis21sdm3.apps.googleusercontent.com',
  projectId: 'cexamen2-6b88e',
  storageBucket: 'examen2-6b88e.appspot.com',
  appId: '1:683471465239:android:b0280f95e31c98d6422552',
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export { auth };

